# ESP32 Espresso Sensors

[Link to Main Project](https://sites.google.com/view/coffee4randy/projects/hx-dumb-to-smart)

[Link to Sensor part](https://sites.google.com/view/coffee4randy/projects/hx-dumb-to-smart/sensor-esp32)

ESP32 program to read sensors on espresso machine.  Displays data on LCD and sends via ESP Now to webserver ESP

Copy these files into the same directory to build with arduino program.  First link above, main sites page, has instructions to install all libraries necessary.

- Main program is ESP32flow08
- Credentials contains wifi passwords and email info
- ota contains function for OTA programming mode
- user.h contains some contants



Program features
 *    read group temperature and boiler temp from TC4 card (I2C interface).  
 *    read water flow rate from a flow senser.  Uses interrupt routinte to count pulses from sensor
 *    calculate total volume from start of flow
 *    read pressure from pressure sensor
 *    keeps timer from start of flow.  Resets time when flow starts, or when button 1 is pushed
 *    sends data via esp now to the ESP32WebSocketSlider 
 *    displays data on 2x16 lcd
 *    supports OTA programming.  Push button 1 (left) at power on to enter OTA mode

