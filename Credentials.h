// Credentials.h
// File to save wifi credentials so don't put them in programs

const char* ssid = "ssid";
const char* password = "passowrd";

//for login to OTA page
//onst char* OTApass = "j2j2j2j2";

//used by ESP32flow program
//mac addresses of ESP32's for espnow broadcast.  Using peer04
uint8_t peer03[] = {0x00,0x00,0x00,0x00,0x00,0x00};
uint8_t peer04[] = {0x00,0x00,0x00,0x00,0x00,0x00};

//used by ESP32WebSocketSlider program
//Used for sending emails
#define emailSenderAccount    "sender@mail.com"
#define emailSenderPassword   "password"
#define emailRecipient        "rx@mail.com"
