/* ESP32flow program by Randy T
 *  Program features
 *    read group temperature and boiler temp from TC4 card (I2C interface).  
 *    read water flow rate from a flow senser.  Uses interrupt routinte to count pulses from sensor
 *    calculate total volume from start of flow
 *    read pressure from pressure sensor
 *    keeps timer from start of flow.  Resets time when flow starts, or when button 1 is pushed
 *    sends data via esp now to the ESP32WebSocketSlider 
 *    displays data on 2x16 lcd
 *    supports OTA programming.  Push button 1 (left) at power on to enter OTA mode
 * 
 * 
 * 
 * 
  Application:
  - Interface water flow sensor with ESP32 board.
  
  Board:
  - ESP32 Dev Module
    https://my.cytron.io/p-node32-lite-wifi-and-bluetooth-development-kit

Randy changes
  Sensor: YF-S401
  https://bc-robotics.com/shop/liquid-flow-meter-yf-s401/
   outputs 5880 pulses per liter (approximately 1 pulse per 0.17mL
 */

 /*
 * ***************Section for defining libraries, constants and global variables
 */
//libs for esp now and wifi
#include <esp_now.h>
#include <esp_wifi.h>
#include <WiFi.h>

//lib for ota programming
#include <AsyncElegantOTA.h>

// libs for i2c and lcd
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#include "Credentials.h" //login info, and other stuff to not make public

// Set your Board ID (ESP32 Sender #1 = BOARD_ID 1, ESP32 flow = BOARD_ID 2, etc)
#define BOARD_ID 2


// these "contributed" libraries must be installed in your sketchbook's arduino/libraries folder
#include <TypeK.h>
#include <cADC.h>

//user.h is in same directory as this file
#include "user.h"
#define BANNER_BRBN "ESP32 Flow"

// LCD interface definition for 16 x 2 LCD at addr 27
LiquidCrystal_I2C lcd(0x27, 16, 2);  

//define digital inputs to read push buttons
#define otaEnPin 39  //gpio pin to read at startup to decide if OTA mode
#define BUTTON01 39
#define BUTTON02 32
#define BUTTON03 33
#define BUTTON04 34

//Define pins for i2c 2-wire interface
#define SDA0_Pin 21
#define SCL0_Pin 22
#define DAC1 25
#define DAC2 26

//define pins to read sensors
#define PRESSURE_SENSOR 36 //to d connector p6
#define FLOW_SENSOR  27    //to d connector p1

//***********Flow cal factor *************************************************
/* need to fix calibration factor  
//double calibrationFactor = 5.880;  
//cal factor for YF-S401 because it outputs 5880 pulses per liter
double calibrationFactor = 83.5;  //adjusted cal factor for YF-S401
*/
//double calibrationFactor = 88.3;  //adjusted cal factor for YF-S401
//88.3*60 = 5298 l/s    
//double calibrationFactor = 5.298;  //adjusted cal factor to get ml/s
double calibrationFactor = 5.298;  //adjusted cal factor to get ml/s

int interval = 1000;        //interval between running loops in main program
long currentMillis = 0;     // used by interval logic to save current time
long previousTime = 0;      //used by interval logic to save time of last loop
long lastflowMillis = 0;     //time from millis for last time flow was >0 (on)

volatile byte pulseCount;  //count pulses as they occur, used by interrupt routine
byte pulse1Sec = 0;  //count pulses in one second
double flowRate;
//double flowMilliLitres;
double totalMilliLitres;
//double PressureSensor;
int buttonValue = 0;  // variable to store the analog input value coming from the switches
float timestamp = 0;
uint32_t nextLoop;
long start_flow = 0;
long flow_time = 0;
boolean flowon_flag = false;
//boolean back_flag = true;
//int button_pushed = 0;
double MeasPres;  //measured pressure
double PresBar;  //pressure converted to bar's
float t1,t2, t_amb;  //temperature's
double WeightBlue = 44.4;  //weight from bluetooth scale
 
//Structure to exchange data over ESP Now
//Must match the receiver structure
typedef struct struct_message {
    int Sid;
    double StempGroup;
    double StempBoiler;
    double Srate;       //ml/s from flowmeter
    double StotalML;    //total ml from flowmeter for this shot
    double Spressure;
    double Sweight;    //placeholder for weight
    int StimerPump;    //not used, used timer from start of flow instead
    int StimerFlow;
    int SreadingId;
} struct_message;


//Create a struct_message called myData
struct_message myData;

// -------------------------- Settings and Values for Button switches and button wiring

#define NOBUTTONVAL 0       // set to this value if no valid switch push detected
#define BUTTONVAL_1 1       // set to this value if Button 1 was pushed
#define BUTTONVAL_2 2       // set to this value if Button 2 was pushed
#define BUTTONVAL_3 3       // set to this value if Button 3 was pushed
#define BUTTONVAL_4 4       // set to this value if Button 4 was pushed


// *************************************************************************************
// NOTE TO USERS: the following parameters should be
// be reviewed to suit your preferences and hardware setup.  

// ------------------ optionally, use I2C port expander for LCD interface
//#define I2C_LCD //comment out to use the standard parallel LCD 4-bit interface
//#define EEPROM_BRBN // comment out if no calibration information stored in 64K EEPROM

#define BAUD 57600  // serial baud rate
#define BT_FILTER 10 // filtering level (percent) for displayed BT
#define ET_FILTER 10 // filtering level (percent) for displayed ET

// needed for usesr without calibration values stored in EEPROM
#define CAL_GAIN 1.00 // substitute known gain adjustment from calibration
#define UV_OFFSET 0 // subsitute known value for uV offset in ADC
#define AMB_OFFSET 0.0 // substitute known value for amb temp offset (Celsius)

// ambient sensor should be stable, so quick variations are probably noise -- filter heavily
#define AMB_FILTER 70 // 70% filtering on ambient sensor readings

// *************************************************************************************

// ------------------------ other compile directives
#define MIN_DELAY 270   // ms between ADC samples (tested OK at 270)
#define NCHAN 2   // number of TC input channels
#define TC_TYPE TypeK  // thermocouple type / library
#define DP 1  // decimal places for output on serial port
#define D_MULT 0.001 // multiplier to convert temperatures from int to float

// --------------------------------------------------------------
// global variables

// class objects
cADC adc( A_ADC ); // MCP3424
ambSensor amb( A_AMB ); // MCP9800
filterRC fT[NCHAN]; // filter for displayed/logged ET, BT
filterRC fRise[NCHAN]; // heavily filtered for calculating RoR
filterRC fRoR[NCHAN]; // post-filtering on RoR values


int32_t temps[NCHAN]; //  stored temperatures are divided by D_MULT
int32_t ftemps[NCHAN]; // heavily filtered temps
int32_t ftimes[NCHAN]; // filtered sample timestamps
int32_t flast[NCHAN]; // for calculating derivative
int32_t lasttimes[NCHAN]; // for calculating derivative

// for ota
const char* host = "esp32 expobar";
// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Create a WebSocket object
AsyncWebSocket ws("/ws");

#define FORMAT_SPIFFS_IF_FAILED true

unsigned int readingId = 0;

/*
 * ************End of defining libraries, constants and global variables
 */


/**********************************************************************************************
 * ********************************** . Section of functions . ********************************
 **********************************************************************************************/
 
// *******************  interrtupt handler routines ******************************

void ICACHE_RAM_ATTR pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}


void ICACHE_RAM_ATTR button1push()
{
  buttonValue = BUTTONVAL_1;
}

void ICACHE_RAM_ATTR button2push()
{
  buttonValue = BUTTONVAL_2;
}


//**************************** Function to find wifi channel ***********************************

int32_t getWiFiChannel(const char *ssid) {
  if (int32_t n = WiFi.scanNetworks()) {
      for (uint8_t i=0; i<n; i++) {
          if (!strcmp(ssid, WiFi.SSID(i).c_str())) {
              return WiFi.channel(i);
          }
      }
  }
  return 0;
}


// *******************  callback when data is sent **********************************
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
//  Serial.print("\r\nLast Packet Send Status:\t");
//  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}
 


//********************* LCD Display Function ****************************
/*format of lcd
 * 0,0 left top row . 0,1 left bottom row 
 *    0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
 * 0  R r . r     P p . p     B  :  t  t  t
 * 1  t t t     V v v . v     G  :  t  t  t      */
 
// LCD output strings
char result3[3],result4[4],st1[6],st2[6];
 
void updateLCD() {

  //limit flow to less then 10
if (flowRate > 10){
  flowRate = 9.9;}

  //write flow rate to LCD
  dtostrf(flowRate, 3, 1, result3); // convert flow (double float) to string with 1 decimal
  lcd.setCursor(0,0);  //start of top row
  lcd.print ("R");
  lcd.print (result3);

if (PresBar < 0){
  PresBar = 0;}
  
  //write Pressure to LCD
  dtostrf(PresBar, 3, 1, result3); // convert flow (double float) to string with 1 decimal
  lcd.setCursor(5,0);  //top row
  lcd.print ("P:");
  lcd.print (result3);
  
// timer from start of flow(rate) bottom left
  flow_time = (millis() - start_flow)/1000;
  int ishot = round( flow_time );
  sprintf( result3, "%3d", flow_time );
  //start of bottom row
  lcd.setCursor(0,1);
//  lcd.print( ssec );
  lcd.print( result3 );
  myData.StimerFlow = ishot;

   //write volume to LCD
  dtostrf(totalMilliLitres, 4, 1, result4); // convert flow (double float) to string with 1 decimal
  lcd.setCursor(4,1);  //bottom row
  lcd.print ("V:");
  lcd.print (result4);
  
  // channel 1 temperature, Boiler
  int it01 = round( t1 );
  if( it01 > 999 ) 
    it01 = 999;
  sprintf( st1, "%3d", it01 );
  lcd.setCursor( 11, 0 );  //right side of top row
  lcd.print("B:");
  lcd.print(st1);

  // channel 2 temperature, Group
  int it02 = round( t2 );
  if( it02 > 999 ) it02 = 999;
  sprintf( st2, "%3d", it02 );
  lcd.setCursor( 11, 1 );  //right side of bottom row
  lcd.print( "G:" ); 
  lcd.print( st2 ); 
  
}

// *******************  routine to read temps . **********************************
void get_samples() // this function talks to the amb sensor and ADC via I2C
{
  int32_t v;
  TC_TYPE tc;
  float tempC;
  
  for( int j = 0; j < NCHAN; j++ ) { // one-shot conversions on both chips
    adc.nextConversion( j ); // start ADC conversion on channel j
    amb.nextConversion(); // start ambient sensor conversion
    delay( MIN_DELAY ); // give the chips time to perform the conversions
    ftimes[j] = millis(); // record timestamp for RoR calculations
    amb.readSensor(); // retrieve value from ambient temp register
    v = adc.readuV(); // retrieve microvolt sample from MCP3424
    tempC = tc.Temp_C( 0.001 * v, amb.getAmbC() ); // convert to Celsius
    v = round( C_TO_F( tempC ) / D_MULT ); // store results as integers
    temps[j] = fT[j].doFilter( v ); // apply digital filtering for display/logging
//    ftemps[j] =fRise[j].doFilter( v ); // heavier filtering for RoR
  }
  t1 = D_MULT*temps[0];
  t2 = D_MULT*temps[1];
};


/**********************************************************************************************
 * ********************************** . Setup and Main loop . *********************************
 **********************************************************************************************/
 
// ********************  Setup section . *****************************
void setup()
{
  Serial.begin(115200);

 // Define pins
  pinMode (otaEnPin, INPUT_PULLUP);
  pinMode (BUTTON02, INPUT_PULLUP);
  pinMode (BUTTON03, INPUT_PULLUP);
  pinMode (BUTTON04, INPUT_PULLUP);
  
 // pinMode(FLOW_SENSOR, INPUT_PULLUP);
  pinMode(PRESSURE_SENSOR, INPUT);


//************** check for OTA mode
if (digitalRead (otaEnPin) == 0) {  //see if OTA button was pushed (left most one)
  // initialize LCD
  lcd.init();
  // turn on LCD backlight                      
  lcd.backlight();
  lcd.setCursor( 0, 0 );
  lcd.print( "OTA Mode" ); //set LCD to say in OTA mode
  OTAWebPage();
}

//if first button was not pushed, run normal setup routine

    // Set device as a Wi-Fi Station and set channel
  WiFi.mode(WIFI_STA);

  int32_t channel = getWiFiChannel(ssid);

  WiFi.printDiag(Serial); // Uncomment to verify channel number before
  esp_wifi_set_promiscuous(true);
  esp_wifi_set_channel(channel, WIFI_SECOND_CHAN_NONE);
  esp_wifi_set_promiscuous(false);
  WiFi.printDiag(Serial); // Uncomment to verify channel change after

  //Init ESP-NOW
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

    // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  //Register peer
  esp_now_peer_info_t peerInfo;
  memcpy(peerInfo.peer_addr, peer04, 6);
  peerInfo.encrypt = false;
  
  //Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
  myData.Sid = BOARD_ID;

  Wire.begin(SDA0_Pin, SCL0_Pin);

  int ValueLCD = 250; //255= 3.3V 128=1.65V
  dacWrite(DAC2, ValueLCD);
  
  // initialize LCD
  lcd.init();                    
  lcd.backlight();    // turn on LCD backlight  
  lcd.setCursor( 0, 0 );
  lcd.print( "starting" ); // display version banner

  amb.init( AMB_FILTER );  // initialize ambient temp filtering

  pulseCount = 0;
  flowRate = 0.0;
 // flowMilliLitres = 0;
  totalMilliLitres = 0;
  previousTime = 0;
  start_flow = millis();

  //button1push() . BUTTON01

  attachInterrupt(digitalPinToInterrupt(FLOW_SENSOR), pulseCounter, FALLING);

  attachInterrupt(digitalPinToInterrupt(BUTTON01), button1push, FALLING);

  attachInterrupt(digitalPinToInterrupt(BUTTON02), button2push, FALLING);

  //button2push()
}


// ***************************** main loop   ************************************** 
void loop()
{
  currentMillis = millis();
  if (currentMillis - previousTime > interval) {
    
    pulse1Sec = pulseCount;
    pulseCount = 0;

    // Because this loop may not complete in exactly 1 second intervals we calculate
    // the number of milliseconds that have passed since the last execution and use
    // that to scale the output. We also apply the calibrationFactor to scale the output
    // based on the number of pulses per second per units of measure (milllitres/sec in
    // this case) coming from the sensor.
    flowRate = ((1000.0 / (millis() - previousTime)) * pulse1Sec) / calibrationFactor;
    
    previousTime = millis();

    if (flowRate > 0) { 
      lastflowMillis = currentMillis;
      if (flowon_flag == false) { //if this is false, flow is now starting
        start_flow = currentMillis;  //reset flow timer
        flowon_flag = true;
        }
      }
   if (currentMillis - lastflowMillis > 20000) {   //if no flow for 20 secs
        totalMilliLitres = 0;
        flowon_flag = false;
    }

    // Divide the flow rate in litres/minute by 60 to determine how many litres have
    // passed through the sensor in this 1 second interval, then multiply by 1000 to
    // convert to millilitres.
   // flowMilliLitres = (flowRate) * 1000;

    // Add the millilitres passed in this second to the cumulative total
    totalMilliLitres += flowRate;
    
    // Print the flow rate for this second in litres / minute
/*    Serial.print("Flow rate: ");
    Serial.print(float(flowRate));  // Print the integer part of the variable
    Serial.print("ml/s");
    Serial.print("\t");       // Print tab space

    // Print the cumulative total of litres flowed since starting
    Serial.print("Output Liquid Quantity: ");
    Serial.print(totalMilliLitres);
    Serial.println("mL / ");     */
    //set structure for sending
    myData.Srate = flowRate;
    myData.StotalML = totalMilliLitres;
/*    if (result == ESP_OK) {
      Serial.println("Sent with success");
    }
    else {
      Serial.println("Error sending the data");
    }*/

    get_samples(); // retrieve temperature values from MCP9800 and MCP3424

    t_amb = amb.getAmbF();      
    //logger(); // output temperature data to serial port

    MeasPres = analogRead (PRESSURE_SENSOR);
    PresBar = (MeasPres-660)/382;  //scale pressure sensor value to bar

    

    updateLCD();  

    myData.Sweight = WeightBlue;
    myData.StempGroup = t2;
    myData.StempBoiler = t1;
    myData.Spressure = PresBar;
    
 //send data structure over espnow to peer04
    esp_err_t result = esp_now_send(peer04, (uint8_t *) &myData, sizeof(myData));

//check for a pushed button  totalMilliLitres = 0;
    if (buttonValue != 0) {
      if (buttonValue == BUTTONVAL_1) {
        start_flow = millis();  //reset flow timer when push first button
      }
      if (buttonValue == BUTTONVAL_2) {
        totalMilliLitres = 0;    //reset shot vol when push 2nd button
      }
      if (buttonValue == BUTTONVAL_3) {
      
      }
      if (buttonValue == BUTTONVAL_4) {
      
      } 
      buttonValue = 0;  //clear button pushed   
    }
  }
}
